const express = require('express')
const app = express()
const port = 3000
app.use(express.json());

data = [
  {
    "id": 0,
    "title": "PHP",
    "image_uri": "https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/320px-PHP-logo.svg.png",
    "description": "PHP is a general-purpose scripting language geared toward web development.",
    "initialVersion": "8.1.9",
    "changelogs": [
      {
        "timestamp": 2022-09-29T15:31:17.558Z,
        "version": "8.1.11",
        "content": "fixed bugs on Core, DOM, FPM, GMP, Intl, PCRE, PDO_PGSQL, Reflection and Streams"
      },
      {
        "timestamp": 2022-09-01T13:28:55.558Z,
        "version": "8.1.10",
        "content": "fixed bugs on Core, Date, DBA, IMAP, Intl, MBString, OPcache, PDO_SQLite, PDO_SQLite3, Streams"
      },
    ]
  },
  {
    "id": 1,
    "title": "JS",
    "image_uri": "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/240px-Unofficial_JavaScript_logo_2.svg.png",
    "description": "JavaScript is a high-level, often just-in-time compiled language that conforms to the ECMAScript standard.",
    "initialVersion": "13 - ECMA-262",
    "changelogs": [

    ]
  },
  {
    "id": 2,
    "title": "docker",
    "image_uri": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Docker_%28container_engine%29_logo.svg/320px-Docker_%28container_engine%29_logo.svg.png",
    "description": "Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.",
    "initialVersion": "20.10.15",
    "changelogs": [
      {
        "timestamp": 2022-05-12T06:08:40.558Z,
        "version": "20.10.16",
        "content": "This release of Docker Engine fixes a regression in the Docker CLI builds for macOS, fixes an issue with docker stats when using containerd 1.5 and up, and updates the Go runtime to include a fix for CVE-2022-29526."
      },
      {
        "timestamp": 2022-06-06T06:06:06.558Z,
        "version": "20.10.17",
        "content": "This release of Docker Engine comes with updated versions of Docker Compose and the containerd, and runc components, as well as some minor bug fixes."
      },
      {
        "timestamp": 2022-09-09T09:05:07.558Z,
        "version": "20.10.18",
        "content": "This release of Docker Engine comes with a fix for a low-severity security issue, some minor bug fixes, and updated versions of Docker Compose, Docker Buildx, containerd, and runc."
      },
    ]
  },
  {
    "id": 3,
    "title": "Java",
    "image_uri": "https://upload.wikimedia.org/wikipedia/fr/thumb/2/2e/Java_Logo.svg/129px-Java_Logo.svg.png",
    "description": "Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.",
    "initialVersion": "Java SE 19",
    "changelogs": [

    ]
  },
  {
    "id": 4,
    "title": "node.js",
    "image_uri": "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/320px-Node.js_logo.svg.png",
    "description": "Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.",
    "initialVersion": "18.9.0",
    "changelogs": [
      {
        "timestamp": 2022-09-28T16:38:46.558Z,
        "version": "18.10.0",
        "content": "Notable changes on doc, gyp, http and stream."
      },
      {
        "timestamp": 2022-06-06T06:06:06.558Z,
        "version": "18.9.1",
        "content": "Multiple CVEs fixe and llhttp updated to 6.0.10."
      },
    ]
  },
  {
    "id": 5,
    "title": "Spring",
    "image_uri": "https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Spring_Framework_Logo_2018.svg/320px-Spring_Framework_Logo_2018.svg.png",
    "description": "The Spring Framework is an application framework and inversion of control container for the Java platform.",
    "initialVersion": "5.3.20",
    "changelogs": [
    {
      "timestamp": 2022-05-11T09:18:55.558Z,
      "version": "5.3.20",
      "content": "14 fixes and improvements."
    },
    ]
  },
  {
    "id": 6,
    "title": "ZAP",
    "image_uri": "https://4.bp.blogspot.com/-oG3PCDp9JWs/VSZjv-WQkgI/AAAAAAAAAYg/KwhC2F3mABM/s1600/zap256x256.png",
    "description": "OWASP Zed Attack Proxy",
    "initialVersion": "2.11.0",
    "changelogs": [
    {
      "timestamp": 2022-09-11T09:18:55.558Z,
      "version": "2.11.1",
      "content": "Changes in Log4j."
    },
    ]
  },
  {
    "id": 7,
    "title": "SFML",
    "image_uri": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/SFML_Logo.svg/130px-SFML_Logo.svg.png",
    "description": "Simple and Fast Multimedia Library",
    "initialVersion": "2.5.1",
    "changelogs": []
  }
]



app.post('/projects', async (req, res) => {
  // check missing parameters
  if (
    !req.body.name ||
    !req.body.initialVersion
  )
  {
    res.status(422).json({
      error: {
        message: 'Missing parameters.'
      }
    })
    return
  }

  // insert new user
  data.push({
    "id": data.length,
    "title": req.body.name,
    "image_uri": req.body.image_uri,
    "initialVersion": req.body.initialVersion,
    "changelogs": []
  })

  res.status(200).json({"data": "done"})
})

app.get('/projects', (req, res) => {
  res.status(200).json(data)
})

app.get('/projects/:projectId', (req, res) => {
  data.forEach( (project) => {
    if (project.id == req.params.projectId) {
      res.status(200).json(project)
      return
    }
  });
})



app.post('/projects/:projectId/changelogs', async (req, res) => {
  // check missing parameters
  if (
    !req.body.version ||
    !req.body.content
  )
  {
    res.status(422).json({
      error: {
        message: 'Missing parameters.'
      }
    })
    return
  }

  // search project
  data.forEach( (project) => {
    if (project.id == req.params.projectId) {
      project.changelogs.push({
        timestamp: new Date(),
        version: req.body.version,
        content: req.body.content
      })
    }
  });

  res.status(200).json({"data": "done"})
})

app.get('/projects/:projectId/changelogs', (req, res) => {
  data.forEach( (project) => {
    if (project.id == req.params.projectId) {
      res.status(200).json(project.changelogs)
      return
    }
  });
})

app.get('/changelogs', (req, res) => {
  let toReturn = []
  for (const projectId in req.body.projectsId) {
    data.forEach( (project) => {
      if (project.id == projectId) {
        project.changelogs.forEach( (changelog) => {
          changelog.projectId = projectId
          changelog.image_uri = project.image_uri
          toReturn.push(changelog)
        })
        return
      }
    })
  }
  res.status(200).json(toReturn)
})

app.listen(port, () => {
  console.log(`MVP app listening on port ${port}`)
})
